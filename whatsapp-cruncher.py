import sys
import re
from datetime import datetime
from emoji import UNICODE_EMOJI
from functools import reduce

# Header that indicates the start of a message
MSG_HEADER = re.compile(r'\d\d\d\d-\d\d-\d\d, \d\d:\d\d - .+: .+')
# Header that indicates the start of a function (change in title, subject,
# etc.)
FUNC_HEADER = re.compile(r'\d\d\d\d-\d\d-\d\d, \d\d:\d\d - .+')

class ItemType:
    MSG = 'msg'
    MEDIA = 'media'
    FUNC = 'func'

class FnType:
    SUBJECT_CHANGE = 'subc'
    ICON_CHANGE = 'iconc'
    PEOPLE_ADD = 'ppla'
    GROUP_CREATE = 'groupc'
    UNKNOWN = 'unb'

class Item:
    header = 'Date,Author,Type (msg media func),Function Type (subc iconc ppla groupc unb),Length'

    def __init__(self):
        self.author = ''
        # Enumeration. The ItemType class
        self.type = ''
        # Enumeration. The FnType class
        self.fntype = ''
        # Content if applicable
        self.content = ''
        # Date and time it happened
        self.instance = None

    def __str__(self):
        items = [self.instance.isoformat(), self.author, self.type,
                 self.fntype, str(len(self.content))]
        return ','.join(items)

class AuthorStats:
    header = 'Name,Messages,Media,Average message length,Average emojis in messages'

    def __init__(self, name):
        self.name = name
        self.msgs = 0
        self.medias = 0
        self.msg_lens = []
        self.emojis = []

    def __str__(self):
        avg_msg_lens = sum(self.msg_lens) / self.msgs
        if sum(self.emojis) > 0:
            avg_emojis = sum(self.emojis) / len([x for x in self.emojis if x > 0])
        else:
            avg_emojis = 0
        return f'{self.name},{self.msgs},{self.medias},{avg_msg_lens},{avg_emojis}'

def count_emojis_in_content(content):
    return len([x for x in content if x in UNICODE_EMOJI])

def get_author_stats(name, items):
    myitems = [x for x in items if x.author == name]
    mymsgs = [x for x in myitems if x.type == ItemType.MSG]
    author = AuthorStats(name)
    author.msgs = len(mymsgs)
    author.medias = len([x for x in myitems if x.type == ItemType.MEDIA])
    author.msg_lens = [len(x.content) for x in mymsgs]
    author.emojis = [count_emojis_in_content(x.content) for x in mymsgs]

    return author

def args_are_valid(argv):
    return len(argv) == 2

def args_use_stdin(argv):
    return argv[1] == '--'

def lines_from_args(argv):
    if args_use_stdin(argv):
        return sys.stdin.readlines()
    else:
        return open(argv[1]).readlines()

def get_line_function(line):
    if 'changed the subject from' in line:
        return FnType.SUBJECT_CHANGE
    elif 'changed this group\'s icon' in line:
        return FnType.ICON_CHANGE
    elif 'added' in line:
        return FnType.PEOPLE_ADD
    elif 'created group' in line:
        return FnType.GROUP_CREATE
    else:
        return FnType.UNKNOWN

def author_from_fnline(fnline, line_fn):
    fnsection = fnline.split(' - ')[1]
    if line_fn == FnType.SUBJECT_CHANGE or line_fn == FnType.ICON_CHANGE:
        return fnsection.split(' changed ')[0]
    elif line_fn == FnType.PEOPLE_ADD:
        return fnsection.split(' added ')[0]
    elif line_fn == FnType.GROUP_CREATE:
        return fnsection.split(' created ')[0]
    else:
        return fnsection

def item_from_msgline(msgline):
    split = msgline.split(': ')
    item = Item()
    item.author = split[0][20:]
    item.content = split[1]
    item.instance = datetime.strptime(msgline[:17], '%Y-%m-%d, %H:%M')
    item.type = ItemType.MEDIA if item.content == '<Media omitted>' else ItemType.MSG
    item.fntype = FnType.UNKNOWN
    return item

def item_from_fnline(fnline):
    item = Item()
    item.type = ItemType.FUNC
    item.fntype = get_line_function(fnline)
    item.author = author_from_fnline(fnline, item.fntype)
    item.instance = datetime.strptime(fnline[:17], '%Y-%m-%d, %H:%M')
    return item

def items_from_args(argv):
    lines = lines_from_args(argv)
    items = []
    tmp_item = None
    for line in lines:
        line = line.rstrip()
        if re.match(MSG_HEADER, line):
            # We add the message if the last message wasn't added before
            if tmp_item is not None:
                items.append(tmp_item)

            tmp_item = item_from_msgline(line)
        elif re.match(FUNC_HEADER, line):
            # We add the message if the last message wasn't added before
            if tmp_item is not None:
                items.append(tmp_item)

            tmp_item = item_from_fnline(line)
        else:
            # We are continuing the last message
            if tmp_item.type == ItemType.MSG:
                tmp_item.content += '\n' + line
            else:
                print(f'warning: multiline msg {{{line}}} following non-message header')

    return items

def print_monthly_authorstats(items, authors):
    print('Month,' + ','.join(authors))
    year = items[0].instance.year
    month = items[0].instance.month

    while (year <= items[-1].instance.year and
           month <= items[-1].instance.month):

        slot_items = [x for x in items if (x.instance.year == year and
                                           x.instance.month == month)]
        author_msgs = map(lambda name: str(get_author_stats(name,
                                                            slot_items).msgs),
                          authors)

        print(f"{year}-{month},{','.join(author_msgs)}")

        if month == 12:
            month, year = 1, year + 1
        else:
            month += 1

def print_accumulated_authorstats(items, authors):
    author_stats = map(lambda name: get_author_stats(name, items), authors)
    print(AuthorStats.header)
    print('\n'.join(map(str, author_stats)))

def print_msg_stats(items):
    print(Item.header)
    print('\n'.join(map(str, items)))

def main(argv):
    if not args_are_valid(argv):
        print('error: please enter file name or -- if you want us to use standard input')
        return

    items = items_from_args(argv)
    # authors = list(set(map(lambda x: x.author, items)))
    # print(f'Time span,{items[0].instance},{items[-1].instance}')
    # print()
    # print_accumulated_authorstats(items, authors)
    # print()
    # print_monthly_authorstats(items, authors)
    print_msg_stats(items)

if __name__ == '__main__':
    main(sys.argv)
