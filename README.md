# Whatsapp Scraper Script

This is a script (or maybe soon, a collection of scripts) that does things to
an exported Whatsapp conversation. To use, simply specify the file name of the
exported Whatsapp conversation.

```sh
# Specify file name directly
python whatsapp-cruncher.py "Whatsapp Conversation for Eason Chan.txt"

# Pipe content from stdin directly
head "Whatsapp Conversation for Eason Chan.txt" | python whatsapp-cruncher.py --
```

The CSV file is output directly to stdout, so you may want to pipe that to
`xclip` or some other file directly to save time in highlighting and pasting
the entire text.

The format of the CSV is as follows:

Name | Description
---|---
Date | Date and time for when the message was sent out (to the minute) in ISO
Author | Whoever wrote the message or action (should be a name in your contacts
Type | The type of message; can be a message, media, or function
Function Type | The type of the function, if applicable
Length | The length of the message, if applicable
